<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('firstname');
            $table->string('lastname');
            $table->string('street');
            $table->date('birthdate');
            $table->string('zip');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('skills')->nullable();
            $table->string('website')->nullable();
            $table->string('tarif')->nullable();
            $table->string('job')->nullable();
            $table->string('phone')->nullable();
            $table->string('role')->default('member');
            $table->boolean('driverlicense')->default(false);
            $table->string('profileimage')->nullable()->default("avatar.png");
            $table->unsignedInteger('city_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
