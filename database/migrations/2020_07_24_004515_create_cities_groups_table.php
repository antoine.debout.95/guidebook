<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('city_id')->nullable()->index();
            $table->unsignedInteger('group_id')->nullable()->index();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_groups');
    }
}
