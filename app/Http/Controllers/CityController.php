<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index(){
        $cities = \App\City::with('countrie')->get();
        return view('city',compact('cities'));
    }

    public function showcitiesincountry($id){
        $cities = \App\City::with('countrie')->where('countrie_id',$id)->get();
        return view('city',compact('cities'));
    }

    public function create(){

    }
    public function store(){
        
    }
    public function show($id){

    }
    public function update($id){

    }
    public function destroy($id){

    }
    public function edit($id){

    }
}
