<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $guides = \App\User::with('city')->get();
        return view('guide',compact('guides'));
    }
    public function create(){

    }
    public function store(){
        
    }
    public function show($id){
        $guide = \App\User::find($id)->with('city')->get();
        return view('guide',compact('guides'));
    }
    public function update($id){

    }
    public function destroy($id){

    }
    public function edit($id){

    }
    public function showguidesincity($city){
        $guides = \App\User::with('city')->where('city_id',$city)->get();
        return view('guide',compact('guides'));
    }
}
