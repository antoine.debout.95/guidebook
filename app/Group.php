<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id'
    ];

    /**
     * Contact user in case of problem
     */
    public function contactuser(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function users(){
        return $this->belongsToMany('App\User', 'groups_users');
    }

    public function cities(){
        return $this->belongsToMany('App\City');
    }
}
