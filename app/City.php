<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'picture', 'description', 'countrie_id'
    ];

    public function countrie(){
        return $this->belongsTo('App\Countrie');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    public function users(){
        return $this->hasMany('App\User');
    }
}
