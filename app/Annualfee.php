<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annualfee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'year', 'date', 'amount', 'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

}
