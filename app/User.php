<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'firstname',
        'lastname',
        'street',
        'birthdate',
        'zip',
        'title',
        'description',
        'skills',
        'website',
        'role',
        'profileimage',
        'city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function annualfees(){
        return $this->hasMany('App\Annualfee');
    }

    /**
     * Group where the user is a contact for problem
     */
    public function contactgroups(){
        return $this->hasMany('App\Group');
    }

    public function groups(){
        return $this->belongsToMany('App\Group','groups_users');
    }

    public function languages(){
        return $this->belongsToMany('App\Language', 'languages_users');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }
}
