<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');
Route::get('/lapland', function () {
    return view('lapland');
})->name('lapland');
Route::get('/aurora', function () {
    return view('aurora');
})->name('aurora');
Route::get('/guide', function () {
    return view('guide');
})->name('guide');
Route::get('/contact', function () {
    return view('contact');
})->name('contact');
Route::get('/about', function () {
    return view('about');
})->name('about');
Route::resource('users', 'UserController');
Route::resource('cities', 'CityController');
Route::get('/cities/citiesincountry/{id}', 'CityController@showcitiesincountry')->name('cities.citiesincountry');
Route::get('/users/guidesincity/{id}', 'UserController@showguidesincity')->name('users.guidesincity');
Auth::routes();
