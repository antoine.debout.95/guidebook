@extends('layout.app')
@section('title')
Guides
@endsection

@section('content')
<section>
    <div class="container card-guide-container">
        @forelse ($guides as $guide)
            <!-- Faire un appel dans le controlleur pour récuperer memberlangage et membergroup -->
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <div class="flip-guide-card">
                        <div class="flip-content">
                            <div class="flip-content-front">
                                <div class="card">
                                    <img src="{{ asset('img/guides/'.$guide->profileimage) }}" class="card-img-top" alt="..." width="300" height="300">
                                    <div class="card-body">
                                        <h4 class="card-title">{{ $guide->name }}</h4>
                                        <h6 class="card-subtitle text-muted">{{ $guide->title }}</h6>
                                        <p class="card-text p-y-1">{{ $guide->description }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="flip-content-back flip-content-back-{{ $guide->id }}" >
                                <h1>{{ $guide->name }}</h1>
                                <br/>
                                <ul class="list-group list-group-flush contact-list-style">
                                    <li class="list-group-item list-group-guide"><img src="{{ asset('img/icon/house.png') }}" width="20" height="20"><span>{{ $guide->street }}, {{ $guide->zip }} {{ $guide->id }}</span></li>
                                    <li class="list-group-item list-group-guide"><img src="{{ asset('img/icon/email.png') }}" width="20" height="20"><span>{{ $guide->email }}</span></li>
                                    <li class="list-group-item list-group-guide"><img src="{{ asset('img/icon/telephone.png') }}" width="20" height="20"><span>{{ $guide->phone }}</span></li>
                                    <li class="list-group-item list-group-guide"><img src="{{ asset('img/icon/euro.png') }}" width="20" height="20"><span>{{ $guide->tarif }}</span></li>
                                    <li class="list-group-item list-group-guide"><img src="{{ asset('img/icon/language.png') }}" width="20" height="20">
                                        <span>
                                            @forelse($guide->languages as $l)
                                                {{ $l->name }}
                                            @empty
                                                This guide speaks no forein language
                                            @endforelse
                                        </span>
                                    </li>
                                </ul><br>
                                <img src="{{ asset('img/guides/qr.png') }}" width="80" height="80" data-toggle="modal" data-target="#modal-box-{{ $guide->id }}">
                            </div>
                        </div>
                </div>
            </div>

            <div class="modal fade" id="modal-box-{{ $guide->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-box-{{ $guide->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg modal-back-guide" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="modal-box-{{ $guide->id }}">{{ $guide->name }} - Guide n°{{ $guide->id }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body modal-body-class">
                            <img src="{{ asset('img/guides/'.$guide->profileimage) }}" class="card-img-top img-card-back" alt="...">
                            <p><ul class="list-group list-group-flush ">
                                <li class="list-group-item modal-card-back"><img src="{{ asset('img/icon/cake.png') }}" width="30" height="30"><span>{{ $guide->birthdate }}</span></li>
                                <li class="list-group-item modal-card-back"><img src="{{ asset('img/icon/profession.png') }}" width="30" height="30"><span>{{ $guide->job }}</span></li>
                                <li class="list-group-item modal-card-back"><img src="{{ asset('img/icon/hobbies.png') }}" width="30" height="30"><span>{{ $guide->skills }}</span></li>
                                <li class="list-group-item modal-card-back"><img src="{{ asset('img/icon/web.png') }}" width="30" height="30"><span>{{ $guide->website }}</span></li>
                                <li class="list-group-item modal-card-back"><img src="{{ asset('img/icon/group.png') }}" width="30" height="30">
                                    <span>
                                        @forelse($guide->groups as $g)
                                            {{ $g->name }}
                                        @empty
                                            This guide is not in any group
                                        @endforelse
                                    </span>
                                </li>
                            </ul></p>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>
        @empty
        <div class="alert alert-warning" style="width:100%;" role="alert">
            Aucun guide pour le moment.
        </div>
        @endforelse
    </div>
</section>
@endsection