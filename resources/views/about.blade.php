@extends('layout.app')
@section('title')
About
@endsection

@section('content')
<section class="profile_area">
    <div class="container">
        <div class="profile_inner p_120">
            <div class="row">
                <div class="col-lg-5">
                    <img class="img-fluid" src="{{asset('img/me.jpg')}}" alt="">
                </div>
                <div class="col-lg-7">
                    <div class="personal_text">
                        <h6>Hello Everybody, i am</h6>
                        <h3>Antoine DEBOUT</h3>
                        <h4>IT Engineer | Web Developper</h4>
                        <p>I'm Antoine DEBOUT and I'm 22 (23 on the 27th December). I'm an exchange student from France, and I live now in Tornio for 3 mounths and a half to study in LapinAmk for the Business IT Degree</p>
                        <ul class="list basic_info" style="list-style:none;margin-left:0;padding-left:0;">
                            <li><a href="#"><i class="lnr lnr-calendar-full"></i> 27th December, 1997</a></li>
                            <li><a href="#"><i class="lnr lnr-phone-handset"></i> +33 6 67 77 80 90</a></li>
                            <li><a href="#"><i class="lnr lnr-envelope"></i> antoine.debout.95@gmail.com</a></li>
                            <li><a href="#"><i class="lnr lnr-home"></i>21, Rue de la Friche, 95520 OSNY - FRANCE</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection