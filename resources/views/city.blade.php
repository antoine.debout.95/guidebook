@extends('layout.app')
@section('title')
Cities
@endsection

@section('content')
<div class="container mycontainer">
    <div class="row">
        @forelse($cities as $city)
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <div class="card" style="margin-bottom:4.5em;">
                <img src="{{ asset('img/cities/'.$city->picture) }}" class="card-img-top" alt="..." width="300" height="300">
                    <div class="card-body">
                        <h3 class="card-title">{{ $city->name }}</h3>
                        <a href="{{ route('cities.citiesincountry', ['id' => $city->countrie->id])}}"><h4 class="card-subtitle text-muted">{{ $city->countrie->name }}</h4></a>
                        <p class="card-text p-y-1">{{ $city->description }}</p>
                        <a href="{{ route('users.guidesincity', ['id' => $city->id])}}" class="btn btn-secondary" style="width:100%;"><span class="lnr lnr-eye"></span> Show Guides</a>
                    </div>
                </div>
            </div>
        @empty
        <div class="alert alert-warning" style="width:100%;" role="alert">
            Aucune ville pour le moment.
        </div>
        @endforelse
    </div>
</div>
@endsection