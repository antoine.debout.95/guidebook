@extends('layout.app')
@section('title')
Aurora
@endsection

@section('content')
<div class="jumbotron jumbotron-fluid my-jumbotron">
    <div class="container ">
    <h1 class="display-4">The Northern Lights</h1>
    <p class="lead">One of the most remarkable features of Finland is light. When the endless sunshine of summer gives way to dark winter, the Northern Lights appear like magic and lighten up the sky.

        The further north you go, the greater the chances of spotting the Aurora Borealis – in Finnish Lapland they can appear on 200 nights a year. In Helsinki and the south, the Aurorae can be seen on roughly 20 nights a winter, away from city lights.</p>
    </div>
</div>
<div class="container aurora-container">
    <div class="gallery-aurora">
        <figure class="gallery_aurora_item gallery_aurora_item_1">
            <img src="{{asset('img/aurora/1.jpg')}}" class="gallery_aurora_img" alt="Img 1">
        </figure>
        <figure class="gallery_aurora_item gallery_aurora_item_2">
            <img src="{{asset('img/aurora/2.gif')}}" class="gallery_aurora_img" alt="Img 2">
        </figure>
        <figure class="gallery_aurora_item gallery_aurora_item_3">
            <img src="{{asset('img/aurora/3.jpg')}}" class="gallery_aurora_img" alt="Img 3">
        </figure>
        <figure class="gallery_aurora_item gallery_aurora_item_4">
            <img src="{{asset('img/aurora/4.jpg')}}" class="gallery_aurora_img" alt="Img 4">
        </figure>
        <figure class="gallery_aurora_item gallery_aurora_item_5">
            <img src="{{asset('img/aurora/5.gif')}}" class="gallery_aurora_img" alt="Img 5">
        </figure>
        <figure class="gallery_aurora_item gallery_aurora_item_6">
            <img src="{{asset('img/aurora/6.jpg')}}" class="gallery_aurora_img" alt="Img 6">
        </figure>
    </div>
</div>
@endsection