@extends('layout.app')
@section('title')
Contact
@endsection

@section('content')
<div class="container mycontainer">
    <div class="row">
        <div class="col-xl-5 mx-auto">
        <h2>Contact</h2>
        <div class="form-area"> 
            <form role="form" action="#">
            <br style="clear:both">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Your Firstname" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Your Email" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Your Number">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Your Object" required>
                </div>
                <div class="form-group">
                <textarea class="form-control" type="textarea" id="message" placeholder="Your Message" maxlength="140" rows="7" required></textarea>                 
                </div>
                <button type="button" name="submit" class="btn btn-dark pull-left"><span class="lnr lnr-envelope"></span> Send Message</button>
                <small id="helpButton" class="form-text text-muted">This features is not enabled yet</small>
            </form>
        </div>
        </div>
        <br/>
        <div class="col-xl-5 mx-auto">
            <div class="row contact_location">
                <h2>Map of our location</h2><br/>
                <div style="width: 100%" class="maps-card"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1632.2428451270107!2d24.137089316364474!3d65.85156067899015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x45d547775acfd3e5%3A0xcc881a0192e7f192!2sLapland%20University%20of%20Applied%20Sciences%20Minerva!5e0!3m2!1sfr!2sfi!4v1570616806126!5m2!1sfr!2sfi" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe></div><br />
            </div>
            <div class="row">
                <div class="col-sm-12 mx-auto contact_info">
                <h2>Information contact</h2><br/>
                <ul class="list-group list-group-flush contact-list-style">
                    <li class="list-group-item"><img src="{{asset('img/icon/house.png')}}" width="20" height="20"><span>Kauppakatu 58, 95400 Tornio</span></li>
                    <li class="list-group-item"><img src="{{asset('img/icon/email.png')}}" width="20" height="20"><span>antoine.debout@edu.lapinamk.fi</span></li>
                    <li class="list-group-item"><img src="{{asset('img/icon/telephone.png')}}" width="20" height="20"><span>+358 020 7986000</span></li>
                    <li class="list-group-item"><img src="{{asset('img/icon/hour.png')}}" width="20" height="20"><span>Mon - Fri | 9:00 - 17:00</span></li>
                </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection