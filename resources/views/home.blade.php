@extends('layout.app')

@section('title')
Homepage
@endsection

@section('content')
<div class="body-index">
    <section>
        <div class="container container-text-index">
            <div class="row">
            <div class="col-lg-8">
                <h1 class="mt-4">Welcome on the Lapland Website</h1>
                <p>Almost uninhabited, covered with vast wilderness, Finnish Lapland is one of the most unique landscapes in Europe, both in summer, under the midnight sun, and in winter, in the blue darkness of the permanent night. . With a few precautions, you will enjoy the beauties of a virgin nature of Finnish Lapland, with unsuspected wealth.</p>
                <a href="{{url('lapland')}}" class="btn btn-secondary btn-block  active" role="button" aria-pressed="true">More about Lapland...</a>
            </div>
            </div>
        </div>
    </section>
</div>
@endsection

