@extends('layout.app')
@section('title')
Lapland
@endsection

@section('content')
<div class="jumbotron jumbotron-fluid my-jumbotron jumbotron-lapland">
    <div class="container ">
    <h1 class="display-4">Lapland - Finland</h1>
    <p class="lead">Lapland is the most northerly region of Finland. Sparsely populated, it borders Sweden, Norway, Russia and the Baltic Sea. It is known for its vast subarctic wilderness, ski resorts and various natural phenomena such as the midnight sun and the aurora borealis. Its capital, Rovaniemi, is the gateway to the region. The homeland of the indigenous Sami people, which extends to neighboring countries, is at the far north.</p>
    </div>
</div>
        
<section class="showcase">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/finland-carousel/6.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>AURORA BOREALIS</h2>
            <p class="lead mb-0">How do the Northern Lights come about? Sami legend says a Fox runs across the Arctic fells and lights up the sky with sparks flying from its tail, whirling up the snow. The modern Finnish term “revontulet”, the fox’s fires, derives from this myth. A scientist’s explanation to the phenomenon would be something like “the solar wind sends charged particles towards the Earth, and upon colliding with its atmosphere they produce energy given off as light”. We prefer the Sami myth.</p>
        </div>
        </div>
        <div class="row no-gutters">
        <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/finland-carousel/4.jpg');"></div>
        <div class="col-lg-6 my-auto showcase-text">
            <h2>HIT THE ROAD</h2>
            <p class="lead mb-0">Santa and Rudolph are an inseparable team, keeping one another company as they travel over rugged fells and through freshly fallen snow.

                It’s essential for Santa’s reindeer to stock up on energy before Christmas Eve, and lichen works wonders. A visit to Santa’s office could end with you stroking the fur of a real reindeer – just be sure to keep an eye open for the one with a red nose.</p>
        </div>
        </div>
        <div class="row no-gutters">
        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/finland-carousel/5.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>LAND OF A THOUSAND LAKES</h2>
            <p class="lead mb-0">Taking a trip to Lakeland is like taking a trip into the heart of Finnish identity. One of the most important birthplaces of the Finnish identity are the deep green forests, the rolling hills and the glittering lakes that cover most of central Finland. Water is a very important element for the Finns. Here, not only the homes, but also millions of saunas and cozy cottages are located close to the water. Finns simply enjoy a life by the water and if you look at the map – who wouldn’t in a land mostly made of it?</p>
        </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/finland-carousel/7.jpg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
            <h2>MIDNIGHT SUN</h2>
            <p class="lead mb-0">In the summer Finnish Lapland bathes in 24-hour sunlight for nearly three months. It is an incredible contrast to the darkest winter months when there is no sun at all. “Midnight sun”, as this phenomenon is called, calls for many activities such as hiking on the beautiful fells or visiting cultural events such as the Midnight Sun Film Festival in Sodankylä.</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/finland-carousel/8.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>THE SAMI</h2>
            <p class="lead mb-0">The Sámi are the only indigenous people of the European Union, residing in the Northern parts of Finland, Norway and Sweden as well as in parts of North-Eastern Russia. In Finland, the Sámi population is approximately 6 000 strong, and the preservation of their endangered language and culture is governed by an autonomous parliament of Inari, Finland.</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6  text-white showcase-img" style="background-image: url('img/finland-carousel/9.jpg');"></div>
            <div class="col-lg-6  my-auto showcase-text">
                <h2>SANTA CLAUS</h2>
                <p class="lead mb-0">Everyone knows the one and only Santa Claus is Finnish and lives in Lapland. However, as the exact location of his hideaway is not known, it is best to head to his Rovaniemi office. This is where he greets visitors all year round.</p>
            </div>
            </div>
    </div>
</section>
@endsection