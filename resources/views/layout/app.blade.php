<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>GuideBook - @yield('title')</title>
        <meta charset="UTF-8">
        <meta name="description" content="Default Layout">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Antoine DEBOUT">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/mystyle.css') }}" >
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" >
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    </head>
    <body>
        @section('sidebar')
            <nav class="navbar navbar-expand-lg navbar-dark static-top mynavclass">
                    <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}"  width="45px" height="40px" style="padding-right:10px;"/>< GUIDE.BOOK /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                        <li>
                            <a class="nav-link" href="{{ url('lapland') }}">Lapland</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ url('aurora') }}">Auroras</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('users.index') }}">Guides</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('cities.index') }}">Cities</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ url('contact') }}">Contact</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ url('about') }}">About</a>
                        </li>
                        <li>
                            <a class="nav-link" href="#">Login</a>
                        </li>
                        </ul>
                    </div>
                    </div>
            </nav>
        @show

        @yield('content')


        <footer id="sticky-footer" class="py-4 bg-dark" style="color:white">
            <div class="container text-center">
              <small>Copyright &copy; Antoine DEBOUT</small>
            </div>
        </footer>
    </body>

    
</html>